package rs.devcenter.battlearena.effectovertime;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class will be used for casting a Dot - Fire Bomb on a Hero.
 * @author Rajkovic Nemanja
 * @version 1.0
 * @date 25.10.2017
 */
public class FireBomb extends EffectOverTime {
    public FireBomb(Hero h1) {
        this.setH1(h1);
        this.setDotHotName("Fire bomb");
        this.setDuration(0);
    }

    /**
     * This method checks round counter and Dot/Hot counter and tickles if needed.
     *
     * @param roundCounter the number of current Round.
     * @param enemyHero Hero on whom Dot/Hot is.
     */
    public void checkDuration(int roundCounter, Hero enemyHero) {
        if (roundCounter < this.getDuration()) {
            spell(enemyHero);
        }
    }

    /**
     * This method will cause damage to the enemy
     *
     * @param hero is a Hero that Dot will be casted upon!
     */
    @Override
    public void spell(Hero hero) {
        if(this.getDuration() == 0){
            System.out.println(getH1().getName() + " casted " + this.getDotHotName());
        }
        if (getH1().getCurrentMana() > 10) {
            getH1().setCurrentMana(getH1().getCurrentMana() - 10);
            hero.setCurrentHP(hero.getCurrentHP() - 10);
            System.out.println("Fire Bomb tickled " +hero.getName()+ " for 10 dmg.");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
        }
    }
}