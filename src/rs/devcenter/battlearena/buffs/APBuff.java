package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will buff up Ability Power of the hero/heroes.
 */
public class APBuff extends Buffs {

    public APBuff(Hero hero) {
        this.setH1(hero);
        this.setBuffName("Ability power Buff");
        this.setDuration(0);
        this.setBuffed(false);
    }

    /**
     * This method checks duration of Buff and remove it if needed.
     *
     * @param roundCounter the number of current round.
     * @param hero         whos buff should be checked.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0) {
            System.out.println(this.getBuffName() + " will be on " + hero.getName() + " for " + (getDuration() - roundCounter) + " rounds");
        }
        if (roundCounter > getDuration() && this.isBuffed() == true) {
            hero.setAP(hero.getAP() - 50);
            this.setBuffed(false);
        }
    }

    /**
     * This methiod will buff the hero which is forwaded.
     *
     * @param h1 Hero to be buffed.
     */
    public void buff(Hero h1) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " buffed his ability power for 50");
            h1.setAP(h1.getAP() + 50);
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setBuffed(true);
        }
    }

    /**
     * This method will buff Hero for certain  amount.
     *
     * @param i is how much Hero's stats will be buffed.
     */
    public static void buff(int i, Hero hero) {
        System.out.println(hero.getName() + "'s ability power is buffed for " + i);
        hero.setAP(hero.getAP() + i);
    }
}