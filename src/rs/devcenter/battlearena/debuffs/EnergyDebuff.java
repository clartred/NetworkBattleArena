package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will de-buff up Energy(Mana) of the hero/heroes.
 */
public class EnergyDebuff extends Debuffs {

    public EnergyDebuff(Hero hero) {
        this.setH1(hero);
        this.setDebuffName("Energy Debuff");
        this.setDuration(0);
        this.setDebuffed(false);
    }

    /**
     * This method checks round counter and buff timer and remove debuff if it is needed.
     *
     * @param roundCounter the number of current Round.
     * @param hero         Hero on whom debuff is.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0) {
            System.out.println(this.getDebuffName() + " will be on " + hero.getName() + " for "+ (getDuration() - roundCounter) + "  rounds");
        }
        if (roundCounter >= getDuration()&& this.isDebuffed() == true) {
            hero.setCurrentMana(((hero.getCurrentMana() + 100)));
            this.setDebuffed(false);
        }
    }

    /**
     * This methiod will de-buff the hero which is forwaded.
     *
     * @param h1 is the Hero who casts de-buff.
     * @param h2 is the Hero that de-buff will be casted on.
     */
    public void debuff(Hero h1, Hero h2) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " debuffed " + h2.getName() + "'s energy by 100");
            h2.setMaxMana(h2.getMaxMana() - 100);
            h2.setCurrentMana(((h2.getCurrentMana() - 100) < 0) ? 0 : (h2.getCurrentMana() - 100));
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setDebuffed(true);
        } else {
            System.out.println(h1.getName() + " doesn't have enough mana");
        }
    }

    /**
     * This method will de-buff Hero for a certain amount.
     *
     * @param i is how much Hero's stats will be de-buffed.
     */
    public static void debuff(int i, Hero hero) {
        System.out.println(hero.getName() + "'s energy is reduced by " + i);
        hero.setMaxMana(hero.getMaxMana() - i);
        hero.setCurrentMana(((hero.getCurrentMana() - i) < 0) ? 0 : (hero.getCurrentMana() - i));
    }
}