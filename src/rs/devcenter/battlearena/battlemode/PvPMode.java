package rs.devcenter.battlearena.battlemode;

import rs.devcenter.battlearena.DBConnection;
import rs.devcenter.battlearena.combat.PvPCombat;
import rs.devcenter.battlearena.hero.Hero;
import rs.devcenter.battlearena.logger.HighScore;
import rs.devcenter.battlearena.network.Client;
import rs.devcenter.battlearena.network.Server;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

/**
 * PvPMode is a class which allows two players to combat
 * until one of them losses his hp.
 */
public class PvPMode extends BattleMode {

    private Hero player1;
    private Hero player2;

    public PvPMode(Hero h1, Hero h2) {
        this.setPlayer1(h1);
        this.setPlayer2(h2);
    }

    /**
     * This method will allow Player's to combat by calling out methods yourTurn() and enemyTurn()
     * until one of them is at or below 0 health.
     */
    public void startCombat() throws IOException, InterruptedException {
        boolean iAmFirst = false;

        try {
            iAmFirst = this.choosePlayerRandom();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //will go on until one of heroes is alive.
        System.out.println(getPlayer1().getName() + " has " + getPlayer1().getCurrentHP() +
                " health and " + getPlayer1().getCurrentMana() + " mana\n");
        System.out.println(getPlayer2().getName() + " has " + getPlayer2().getCurrentHP() +
                " health and " + getPlayer2().getCurrentMana() + " mana\n");

        PvPCombat pvPCombat = new PvPCombat(getPlayer1(), getPlayer2());

        //if player(you) is playing first, first IF statement will occur
        //which will allow him to start first, otherwise, else statement will occur.
        if (iAmFirst == true) {
            while (getPlayer1().getCurrentHP() > 0 & getPlayer2().getCurrentHP() > 0) {
                pvPCombat.yourTurn();
                if (checkHP() == true) {
                    break;
                }
                pvPCombat.enemyTurn();
                if (checkHP() == true) {
                    break;
                }
            }
        } else {
            while (getPlayer1().getCurrentHP() > 0 & getPlayer2().getCurrentHP() > 0) {
                pvPCombat.enemyTurn();
                if (checkHP() == true) {
                    break;
                }
                pvPCombat.yourTurn();
                if (checkHP() == true) {
                    break;
                }
            }
        }
        System.out.println("Number of rounds was: " + pvPCombat.getRoundCounter());
        Thread.sleep(4000);
        DBConnection dbConnection = new DBConnection();
        //Saving User's score in Database.
        try {
            dbConnection.updateTable(getPlayer1(), getPlayer1().getHeroClass());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        HighScore highScore = new HighScore();
        //getting high score from txt logs.
        highScore.gettingHighScore();
        System.exit(0);
    }

    /**
     * This method will check Players health.
     * If any player's health is below 1, it will return false,
     * otherwise, it will return true.
     * @return true/false.
     */
    public boolean checkHP() {
        if (getPlayer1().getCurrentHP() <= 0) {
            getPlayer2().setCurrentLv(getPlayer2().getCurrentLv() + 1);
            System.out.println(getPlayer1().getName() + " won!");
            return true;
        } else if (getPlayer2().getCurrentHP() <= 0) {
            //giving XP to the User if he won
            getPlayer1().setCurrentLv(getPlayer1().getCurrentLv() + 1);
            System.out.println(getPlayer1().getName() + " won!");
            return true;
        } else return false;
    }

    /**
     * This method is creating random integer, sending it to the enemy's server
     * and receiving enemy value from his server.
     * By comparing two integers, method decides who plays first.
     * If return value is true You are playing first,
     * otherwise, your opponent is playing first.
     *
     * @return true or false.
     */
    public boolean choosePlayerRandom() throws InterruptedException, IOException {

        System.out.println("Now Game will randomly choose if You're playing first or second");
        Thread.sleep(5000);
        Random random = new Random();
        int ourRandomInt = random.nextInt(1500) + 10;
        Client.sendData(ourRandomInt + "");
        Thread.sleep(1500);
        int enemyRandomInt = 0;
        while (enemyRandomInt == 0) {
            enemyRandomInt = Server.getServerNumber();
            Thread.sleep(1500);
        }
        boolean iAmFirst;
        System.out.println(ourRandomInt + " Is your Number \n" + enemyRandomInt + " is enemy Number");
        Server.setServerString(null);
        Server.setServerNumber(0);
        if (ourRandomInt == enemyRandomInt) {
            return choosePlayerRandom();
        } else if (ourRandomInt > enemyRandomInt) {
            System.out.println(player1.getName() + " is first and\n " + player2.getName() + " is second");
            iAmFirst = true;
        } else {
            System.out.println(player2.getName() + " is first and\n " + player1.getName() + " is second");
            iAmFirst = false;
        }
        return iAmFirst;
    }

    public Hero getPlayer1() {
        return player1;
    }

    public void setPlayer1(Hero player1) {
        this.player1 = player1;
    }

    public Hero getPlayer2() {
        return player2;
    }

    public void setPlayer2(Hero player2) {
        this.player2 = player2;
    }
}