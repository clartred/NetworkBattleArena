package rs.devcenter.battlearena.network;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Client is a class which is allowing user to
 * send data to the server.
 */
public class Client extends Thread {


    private static Socket clientSocket;
    private static DataOutputStream outToServer;

    public Client(int port, String ip) throws IOException {
        clientSocket = new Socket(ip, port);
    }

    /**
     * This method will send data to a server.
     * @param message the string to be forwarded.
     */
    public static void sendData(String message) throws IOException {
        outToServer = new DataOutputStream(clientSocket.getOutputStream());
        outToServer.writeBytes(message + '\n');
        outToServer.flush();
    }
}