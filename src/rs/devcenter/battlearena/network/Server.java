package rs.devcenter.battlearena.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server is a class which extends Thread class
 * in order to allow server to loop unhindered.
 */
public class Server extends Thread {

    private static BufferedReader serverInput;
    private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static int serverNumber;
    private static String serverString;
    private String message;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }


    @Override
    /**
     * run() is a method which is inherited from Thread class
     * and it's current purpose is to allow server to loop
     * and to receive data from client socket.
     */
    public void run() {
        clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                serverInput = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                message = serverInput.readLine();
            } catch (IOException e) {

            }
            try {
                //we try to parse integer from string
                //if that is not possible, then we simply
                //put it into a string and we use it.
                serverNumber = Integer.parseInt(message);
            } catch (NumberFormatException e) {
                serverString = message;
            }
        }
    }

    public static int getServerNumber() {
        return serverNumber;
    }

    public static void setServerNumber(int serverNumber) {
        Server.serverNumber = serverNumber;
    }

    public static String getServerString() {
        return serverString;
    }

    public static void setServerString(String serverString) {
        Server.serverString = serverString;
    }
}