package rs.devcenter.battlearena;

import rs.devcenter.battlearena.buffs.ADBuff;
import rs.devcenter.battlearena.buffs.APBuff;
import rs.devcenter.battlearena.buffs.EnergyBuff;
import rs.devcenter.battlearena.buffs.HealthBuff;
import rs.devcenter.battlearena.debuffs.ADDebuff;
import rs.devcenter.battlearena.debuffs.APDebuff;
import rs.devcenter.battlearena.debuffs.EnergyDebuff;
import rs.devcenter.battlearena.debuffs.HealthDebuff;
import rs.devcenter.battlearena.hero.Hero;

/**
 * @author Nemanja Rajkovic
 * Class Arena is going to create an ilusion of Arena.
 * For instance, will buff or debuff heroes to make their combat
 * easyer or harder.
 */
public class Arena {

    /**
     * This method will de-buff a Hero, hence create illusion of Demoted Arena.
     *
     * @param hero to be de-buffed.
     * @return hero to be returned.
     */
    public Hero DemoteArena(Hero hero) {
        ADDebuff.debuff(30, hero);
        APDebuff.debuff(30, hero);
        EnergyDebuff.debuff(150, hero);
        HealthDebuff.debuff(100, hero);
        return hero;
    }

    /**
     * This method will buff a Hero, hence create illusion of Enhanced Arena.
     *
     * @param hero to be buffed.
     * @return hero to be returned.
     */
    public Hero EnhancedArena(Hero hero) {
        ADBuff.buff(50, hero);
        APBuff.buff(50, hero);
        EnergyBuff.buff(200, hero);
        HealthBuff.buff(200, hero);
        return hero;
    }
}