package rs.devcenter.battlearena.combat;

import rs.devcenter.battlearena.hero.Hero;
import rs.devcenter.battlearena.network.Client;
import rs.devcenter.battlearena.network.Server;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

/**
 * This class is allowing Player to make a turn and will wait
 * for opponent to make his turn.
 */
public class PvPCombat extends Combat {

    private Hero hero1;
    private Hero hero2;
    private Random random;
    private Scanner scanner;

    public PvPCombat(Hero h1, Hero h2) {
        this.setHero1(h1);
        this.setHero2(h2);
        scanner = new Scanner(System.in);
        random = new Random();

    }

    /**
     * This method will take Player's input and forward it to the client.
     */
    public void yourTurn() throws IOException, InterruptedException {
        Thread.sleep(1000);
        System.out.println(getHero1().getName() + " health is " + getHero1().getCurrentHP() + " " + getHero2().getName() + " health is " + getHero2().getCurrentHP());
        System.out.print("\nPress 1 to cast " + getHero1().getHeroSpells(0).getName() + ", 2 for " + getHero1().getHeroSpells(1).getName()
                + ", 3 for " + getHero1().getHeroSpells(2).getName());
        System.out.println(", 4 to cast " + getHero1().getEffectsOverTime().getDotHotName() + ", 5 for " + getHero1().getBuff().getBuffName()
                + ", 6 for " + getHero1().getDebuff().getDebuffName());

        int i = scanner.nextInt();

        if (i < 1 | i > 6) {
            i = random.nextInt(6) + 1;
        }
        Client.sendData(i + "");
        Thread.sleep(1000);
        //checking dot/hots counter
        getHero1().getEffectsOverTime().checkDuration(getRoundCounter(), getHero2());
        //checking buff counter
        getHero1().getBuff().checkDuration(getRoundCounter(), getHero1());
        //checking debuff counter
        getHero1().getDebuff().checkDuration(getRoundCounter(), getHero2());
        if (i == 1) {
            getHero1().getHeroSpells(0).spell(getHero2());
        } else if (i == 2) {
            getHero1().getHeroSpells(1).spell(getHero2());
        } else if (i == 3) {
            getHero1().getHeroSpells(2).spell(getHero2());
        } else if (i == 4) {
            getHero1().getEffectsOverTime().spell(getHero2());
            getHero1().getEffectsOverTime().setDuration(getRoundCounter() + 3);
        } else if (i == 5) {
            getHero1().getBuff().buff(getHero1());
            getHero1().getBuff().setDuration(getRoundCounter() + 5);
        } else if (i == 6) {
            getHero1().getDebuff().debuff(getHero1(), getHero2());
            getHero1().getDebuff().setDuration(getRoundCounter() + 5);
        } else {
            System.out.println("You picked wrong number ");
        }
        setRoundCounter(getRoundCounter() + 1);
        //mana regeneration
        getHero1().setCurrentMana(getHero1().getCurrentMana() + (10 + getHero1().getCurrentLv() * 5));
        System.out.println("\n" + getHero1().getName() + " health is " + getHero1().getCurrentHP() + "\n");
        System.out.println(getHero2().getName() + " health is " + getHero2().getCurrentHP() + "\n");
        System.out.println("------------------------");
    }

    /**
     * this method will wait for opponent to send his input
     * and then use it to cast a magic.
     */
    public void enemyTurn() throws InterruptedException {

        //checking dot/hots counter
        getHero2().getEffectsOverTime().checkDuration(getRoundCounter(), getHero1());
        //checking buff counter
        getHero2().getBuff().checkDuration(getRoundCounter(), getHero2());
        //checking debuff counter
        getHero2().getDebuff().checkDuration(getRoundCounter(), getHero1());
        Thread.sleep(2000);

        int n = 0;
        //here we wait until we receive opponent's input.
        while (n == 0) {
            Thread.sleep(1500);
            n = Server.getServerNumber();
        }
        Server.setServerNumber(0);
        Server.setServerString(null);
        //if player's input is not relating to any spell
        // method will take random int as his input.
        if (n < 1 | n > 6) {
            n = random.nextInt(5) + 1;
            System.out.println("It looks like player 2 didn't cast a magic so we had to random choose it " + n);
        }
        if (n == 1) {
            getHero2().getHeroSpells(0).spell(getHero1());
        } else if (n == 2) {
            getHero2().getHeroSpells(1).spell(getHero1());
        } else if (n == 3) {
            getHero2().getHeroSpells(2).spell(getHero1());
        } else if (n == 4) {
            getHero2().getEffectsOverTime().spell(getHero1());
            getHero2().getEffectsOverTime().setDuration(getRoundCounter() + 3);
        } else if (n == 5) {
            getHero2().getBuff().buff(getHero2());
            getHero2().getBuff().setDuration(getRoundCounter() + 5);
        } else if (n == 6) {
            getHero2().getDebuff().debuff(getHero2(), getHero1());
            getHero2().getDebuff().setDuration(getRoundCounter() + 5);
        }
        //mana regeneration
        getHero2().setCurrentMana(getHero2().getCurrentMana() + (10 + getHero2().getCurrentLv() * 5));
        System.out.println("--------------------------");
    }


    public Hero getHero1() {
        return hero1;
    }

    public void setHero1(Hero hero1) {
        this.hero1 = hero1;
    }

    public Hero getHero2() {
        return hero2;
    }

    public void setHero2(Hero hero2) {
        this.hero2 = hero2;
    }

}
