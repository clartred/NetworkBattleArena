package rs.devcenter.battlearena.combat;

import rs.devcenter.battlearena.hero.*;
import java.util.Random;
import java.util.Scanner;

/**
 * This is a abstract class Combat which will be inherited by all other types of Combat.
 * It is used to simulate different kinds of combat between User and AI.
 *
 * @author Nemanja Rajkovic
 */
public abstract class Combat {
    private Hero hero1, hero2;
    private int roundCounter;

    public Hero getHero1() {
        return hero1;
    }

    public void setHero1(Hero hero1) {
        this.hero1 = hero1;
    }

    public Hero getHero2() {
        return hero2;
    }

    public void setHero2(Hero hero2) {
        this.hero2 = hero2;
    }

    public int getRoundCounter() {
        return roundCounter;
    }

    public void setRoundCounter(int roundCounter) {
        this.roundCounter = roundCounter;
    }
}