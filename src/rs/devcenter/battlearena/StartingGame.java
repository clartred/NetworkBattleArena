package rs.devcenter.battlearena;

import rs.devcenter.battlearena.battlemode.PvPMode;
import rs.devcenter.battlearena.hero.*;
import rs.devcenter.battlearena.logger.BattleLogger;
import rs.devcenter.battlearena.network.Client;
import rs.devcenter.battlearena.network.Server;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * This class lets User choose which spells he wants to use, which champion
 * he wants to play and what arena he wants to play in.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @date 30.10.2017
 */
public class StartingGame {

    private static Thread server;
    private static Thread client;
    private DBConnection dbConnection = new DBConnection();
    private Hero hero1, hero2;
    private Scanner scanner = new Scanner(System.in);

    /**
     * This method allow players to choose which Hero they want to play.
     */
    public void chooseHero() throws SQLException, InterruptedException, IOException {

        System.out.println("Please choose a Class you want to play: \nPress: " +
                "1 for Priest, 2 for Mage, 3 for Warlock, 4 for Hunter, 5 for Shaman");
        int i = scanner.nextInt();
        Client.sendData(i + "");
        Thread.sleep(2000);
        int j = 0;
        while (j == 0) {
            Thread.sleep(1000);
            j = Server.getServerNumber();
        }
        //again sets server values to null/0
        //because without it, we might get value we don't want.
        Server.setServerNumber(0);
        Server.setServerString(null);
        //retrieving "heroes" from Database.

        if (i == 1) {
            String[] stats = dbConnection.retrieveHero("priest");
            Priest priest = Priest.getInstance(stats[0], Integer.parseInt(stats[1]), Double.parseDouble(stats[2]));
            hero1 = priest;
        } else if (i == 2) {
            String[] stats = dbConnection.retrieveHero("mage");
            Mage mage = Mage.getInstance(stats[0], Integer.parseInt(stats[1]), Double.parseDouble(stats[2]));
            hero1 = mage;
        } else if (i == 3) {
            String[] stats = dbConnection.retrieveHero("warlock");
            Warlock warlock = Warlock.getInstance(stats[0], Integer.parseInt(stats[1]), Double.parseDouble(stats[2]));
            hero1 = warlock;
        } else if (i == 4) {
            String[] stats = dbConnection.retrieveHero("hunter");
            Hunter hunter = Hunter.getInstance(stats[0], Integer.parseInt(stats[1]), Double.parseDouble(stats[2]));
            hero1 = hunter;
        } else if (i == 5) {
            String[] stats = dbConnection.retrieveHero("shaman");
            Shaman shaman = Shaman.getInstance(stats[0], Integer.parseInt(stats[1]), Double.parseDouble(stats[2]));
            hero1 = shaman;
        }

        if (j == 1) {
            String[] stats = dbConnection.retrieveHero("priest");
            Priest priest = Priest.getInstance(stats[0], hero1.getCurrentLv(), Double.parseDouble(stats[2]));
            hero2 = priest;
        } else if (j == 2) {
            String[] stats = dbConnection.retrieveHero("mage");
            Mage mage = Mage.getInstance(stats[0], hero1.getCurrentLv(), Double.parseDouble(stats[2]));
            hero2 = mage;
        } else if (j == 3) {
            String[] stats = dbConnection.retrieveHero("warlock");
            Warlock warlock = Warlock.getInstance(stats[0], hero1.getCurrentLv(), Double.parseDouble(stats[2]));
            hero2 = warlock;
        } else if (j == 4) {
            String[] stats = dbConnection.retrieveHero("hunter");
            Hunter hunter = Hunter.getInstance(stats[0], hero1.getCurrentLv(), Double.parseDouble(stats[2]));
            hero2 = hunter;
        } else if (j == 5) {
            String[] stats = dbConnection.retrieveHero("shaman");
            Shaman shaman = Shaman.getInstance(stats[0], hero1.getCurrentLv(), Double.parseDouble(stats[2]));
            hero2 = shaman;
        } else {
            String[] stats = dbConnection.retrieveHero("shaman");
            Shaman shaman = Shaman.getInstance(stats[0], Integer.parseInt(stats[1]), Double.parseDouble(stats[2]));
            hero2 = shaman;
        }
        chooseAName();
    }

    public void chooseAName() throws IOException, InterruptedException {
        System.out.println("Enter your name: For example: Clartred, Nixxiom, Belular, Nobbel");
        String yourName = scanner.next();
        Client.sendData(yourName);
        Thread.sleep(6000);
        String enemyName = null;
        while (enemyName == null) {
            enemyName = Server.getServerString();
            Thread.sleep(1500);
        }
        Server.setServerNumber(0);
        Server.setServerString(null);
        System.out.println("Your name is: " + yourName);
        System.out.println("Enemy name is: " + enemyName);
        hero1.setName(yourName);
        hero2.setName(enemyName);
        try {
            this.chooseArena();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will allow players to choose which Arena they want to play.
     */
    public void chooseArena() throws IOException, InterruptedException {
        System.out.println("Choose Arena you want: 1 for Basic Arena, 2 for Demote Arena, 3 for Enhanced Arena");
        int j = scanner.nextInt();
        Client.sendData(j + "");
        //allow thread to wait 1.5 seconds so opponent's choise
        //can arrive on time.
        Thread.sleep(1500);
        int i = 0;
        while (i == 0) {
            Thread.sleep(1500);
            i = Server.getServerNumber();
        }
        //set serverInt and serverString to null/0 so the
        //next time u use them, they won't hold their last value
        Server.setServerNumber(0);
        Server.setServerString(null);

        //if players didnt agreed to play certain arena
        // basic arena will be played aka. no buffs/debuffs
        // (pros or cons) will be casted on heroes
        if (i != j) {
            j = 1;
        }

        if (j == 2) {
            Arena arena = new Arena();
            hero1 = arena.DemoteArena(hero1);
            hero2 = arena.DemoteArena(hero2);
        } else if (j == 3) {
            Arena arena = new Arena();
            hero1 = arena.EnhancedArena(hero1);
            hero2 = arena.EnhancedArena(hero2);
        }
        startCombat();
    }

    public void startCombat() throws IOException {
        PvPMode pvPMode = new PvPMode(hero1, hero2);
        try {
            pvPMode.startCombat();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will allow User to open the Server and Client socket by asking him
     * for his opponent's port, his port and his opponent's server IP.
     * And will start server and client as separate threads from main's.
     */
    public void startGame() {
        dbConnection.checkIfDBExists();
        BattleLogger battleLogger = new BattleLogger();
        battleLogger.logging();
        System.out.println("Please enter the Server port number(your server's), for example 8888");
        int myPort = scanner.nextInt();
        try {
            server = new Server(myPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //start server thread separate from main thread
        server.start();
        System.out.println("Please enter the IP of the server (Your opponent's), for example 192.168.1.1");
        String ip = scanner.next();
        System.out.println("Please enter your opponent's server port number");
        int enemyPort = scanner.nextInt();
        try {
            //allow thread to wait for 5 seconds in order
            //to make sure that opponent has started their server
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            client = new Client(enemyPort, ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //start client thread as separate thread from server or main thread.
        client.start();
        try {
            this.chooseHero();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}