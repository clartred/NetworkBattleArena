package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;


public class ImageOfMedvieh extends Spell  {

    public ImageOfMedvieh (Hero h1) {
        this.setH1(h1);
        this.setName("Image of Medvieh");
        this.setManaCost(30+(15*h1.getCurrentLv()));
        this.setDamage(30+h1.getAP());
    }

    /**
     * Image of Medvieh is a medium cost, medium damage spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Image of Medvih for  " +  getDamage() + " damage!");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}