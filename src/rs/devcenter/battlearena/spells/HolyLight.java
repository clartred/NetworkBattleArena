package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class HolyLight extends Spell {

    public HolyLight(Hero h1) {
        this.setH1(h1);
        this.setName("Holy light");
        this.setManaCost(20+(15*h1.getCurrentLv()));
        this.setDamage(35+h1.getAP());
    }

    /**
     * Penance is a healing spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            getH1().setCurrentHP(getH1().getCurrentHP() + getDamage());
            System.out.println(getH1().getName() + " healed himself for " + getDamage() + " up to " +getH1().getCurrentHP() + " health");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana.");
        }
        System.out.println("In the light - We are one.");
    }
}