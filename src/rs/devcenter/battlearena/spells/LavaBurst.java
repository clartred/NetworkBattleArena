package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;


public class LavaBurst extends Spell {
    public LavaBurst(Hero h1) {
        this.setH1(h1);
        this.setName("Lava Burst");
        this.setManaCost(30+(15*h1.getCurrentLv()));
        this.setDamage(25+h1.getAP());
    }

    /**
     * Lava Burst spell is a high-damage spell with low energy cost.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {

            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());

            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());

            System.out.println(getH1().getName() + " casted Lava Burst for " + getDamage() + " damage!");
        } else {
            System.out.println(getH1().getName() + " does't have enough mana ");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}
